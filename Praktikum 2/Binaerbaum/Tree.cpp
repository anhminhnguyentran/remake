//
// Created by Tran Anh Minh on 20/03/2021.
//

/*************************************************
* ADS Praktikum 2.2
* Tree.cpp
* Erweiterung um Hilfsfunktionen gestattet.
*************************************************/
#include "Tree.h"
#include "TreeNode.h"
#include <iostream>
#include <iomanip>

using namespace std;

////////////////////////////////////
// Ihr Code hier:
Tree::Tree(){
    anker = nullptr;
    node = nullptr;
    NodeIDCounter = 0;
};
void Tree:: addNode(string Name, int Alter, double Einkommen, int PLZ){
    int PosID = Alter + PLZ + Einkommen;
    TreeNode*Node = new TreeNode(PosID, 0, Name, Alter, Einkommen, PLZ);
    if(anker == nullptr) {
        anker = Node;
    }else{
        TreeNode* tmp = anker;
        TreeNode* pretmp = tmp;
        while(tmp!= nullptr){
            pretmp = tmp;
            if(Node->getNodePosID()>tmp->getNodePosID())
                tmp = tmp->getRight();
            else
                tmp = tmp->getLeft();
        }
        if(Node->getNodePosID()>pretmp->getNodePosID())
            pretmp->setRight(Node);
        else
            pretmp->setLeft(Node);
        Node->setNodeID(++this->NodeIDCounter);
    }
}
void Tree::deleteNode(int NodePosID){
    /**
     *  Falls der Baum leer ist
     */
    if(anker == nullptr) {
        cout << "Der Baum ist schon leer!!!" << endl;
        return;
    }
    /**
     * Falls keinen Node gefunden kann
     */
   else if(!findNode2(NodePosID)) {
        cout << "keinen Node gefunden!!!" << endl;
        return;
    }
    /**
     *  Node und seine Eltern finden
     */
    TreeNode* tmp = anker;
    TreeNode* Node = new TreeNode;
    TreeNode* parent = new TreeNode;
    while(true){
        if(tmp->getNodePosID() == NodePosID) {
            Node = tmp;
            break;
        }
        if(tmp->getLeft() == nullptr && tmp->getRight() == nullptr){
            break;
        }
        else if((tmp->getRight() != nullptr) && (NodePosID > tmp->getNodePosID())){
            parent = tmp;
            tmp = tmp->getRight();
        }
        else if ((tmp->getLeft() != nullptr) && (NodePosID< tmp->getNodePosID())) {
            parent = tmp;
            tmp = tmp->getLeft();
        }
    }
    /**
     *  1. Falls Node ein Blatt
     */
    if (Node->getLeft() == nullptr && Node->getRight() == nullptr){
        if(parent->getLeft() == Node)
            parent->setLeft(nullptr);
        else
            parent->setRight(nullptr);
        if(Node == anker)
            anker = nullptr;
    }
    /**
     * 3. Falls Node 2 Einfolge hat
     */
    else if ((Node->getLeft() != nullptr) && (Node->getRight()!= nullptr)){
        //Find Min und seines Elters des Rechten Teilbaumes
        TreeNode* min = Node->getRight();
        TreeNode* min_parent = Node;
        while(min->getLeft()!= nullptr) {
            min_parent = min;
            min = min->getLeft();
        }

        //Node durch Min ersetzen
        min->setLeft(Node->getLeft());
        TreeNode*min_right = min->getRight();
        if(Node->getRight() == min);
        else
            min->setRight(Node->getRight());
        if(parent->getLeft() == Node)
            parent->setLeft(min);
        else
            parent->setRight(min);

        //min Elter zeigt auf nullptr oder rechte Nachfolge von Min

        if(min->getRight()!= nullptr)
            min_parent->setLeft(min_right);
        else
            min_parent ->setLeft(nullptr);
        //Falls anker lösen
        if(Node == anker)
            anker = min;
    }
    /**
     * 2. Fallse Node 1 Einfolge hat
     */
    else{
        if(Node == anker){
            if(Node->getLeft() != nullptr)
                anker = Node->getLeft();
            else
                anker = Node->getRight();
            return;
        }
        if (parent->getLeft() == Node) {
            if (Node->getLeft() != nullptr)
                parent->setLeft(Node->getLeft());
            else
                parent->setLeft(Node->getRight());
        } else {
            if (Node->getLeft() != nullptr)
                parent->setRight(Node->getLeft());
            else
                parent->setRight(Node->getRight());
        }
    }

    delete Node;
    NodeIDCounter-=1;
}

void Tree:: preoder(TreeNode* anker) {
    if (anker == nullptr)
        cout << "Der Baum ist leer!" << endl;
    else {
        anker->print();
        if (anker->getLeft() != nullptr)
            preoder(anker->getLeft());
        if (anker->getRight() != nullptr)
            preoder(anker->getRight());
    }
}

bool Tree:: findNode1(TreeNode*anker, string Name) {
    if (anker == nullptr)
        return false;
    if (anker->getName() == Name){
        node = anker;
        return true;}
    /*if ((check == NodeIDCounter) && (anker->getName() != Name))
        return false;*/
    if (anker->getLeft() != nullptr){
        node = anker->getLeft();
        if(findNode1(anker->getLeft(), Name))
            return true;
    }
    if (anker->getRight() != nullptr) {
        node = anker->getRight();
        if (findNode1(anker->getRight(), Name))
            return true;
    }
    return false;
}

bool Tree::findNode2(int PosID){
    TreeNode* tmp = anker;
    while(tmp!= nullptr){
        if(tmp->getNodePosID() == PosID)
            return true;
        if(PosID > tmp->getNodePosID())
            tmp = tmp->getRight();
        else
            tmp = tmp->getLeft();
    }
    return false;
}

bool Tree::searchNode(string Name){
   if(findNode1(anker, Name))
       return true;
   return false;
}
void Tree::printAll(){
   preoder(this->anker);
}



//
////////////////////////////////////