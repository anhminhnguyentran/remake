//
// Created by Tran Anh Minh on 24/03/2021.
//

#include "Tree.h"
#include "TreeNode.h"
#include <iostream>
#include <queue>
#include <iomanip>

using namespace std;

////////////////////////////////////
// Ihr Code hier:
Tree::Tree(){
    anker = nullptr;
    node = nullptr;
    NodeIDCounter = 0;
};
/**
 * Top-Down einfügen in Rot Schwarz Baum: wenn 4-Knoten auf den Suchpfad tritt, umwandeln 4-Knoten in 2-Knoten
 * @param Name
 * @param Alter
 * @param Einkommen
 * @param PLZ
 * @return
 */
bool Tree:: addNode(string Name, int Alter, double Einkommen, int PLZ) {
    int PosID = Alter + PLZ + Einkommen;
    TreeNode *Node = new TreeNode(PosID, 0, Name, Alter, Einkommen, PLZ);

    if (anker == nullptr) {
        Node->setRed(false);
        anker = Node;
        return true;
    }
    TreeNode *tmp = anker;
    TreeNode *pretmp = tmp;

    while (tmp != nullptr) {
        pretmp = tmp;
        split4Node(pretmp);
        if (Node->getNodePosID() > tmp->getNodePosID()) {
            tmp = tmp->getRight();
        } else
            tmp = tmp->getLeft();
    }
    if (Node->getNodePosID() > pretmp->getNodePosID())
        pretmp->setRight(Node);
    else
        pretmp->setLeft(Node);

    Node->setNodeID(++this->NodeIDCounter);
    Node->setRed(true);

    if (pretmp->getRed()){
        if (pretmp->getRight() == Node)
            rotateTreeLeft(pretmp, Node);
        else if(pretmp->getLeft() == Node)
            rotateTreeRight(pretmp, Node);
    }
    return false;
}

bool Tree::searchNode(string Name){
    if(findNode1(anker, Name))
        return true;
    return false;
}

void Tree:: preoder(TreeNode* anker) {
    if (anker == nullptr)
        cout << "Der Baum ist leer!" << endl;
    else {
        anker->print();
        if (anker->getLeft() != nullptr)
            preoder(anker->getLeft());
        if (anker->getRight() != nullptr)
            preoder(anker->getRight());
    }
}

bool Tree:: findNode1(TreeNode*anker, string Name) {
    if (anker == nullptr)
        return false;
    if (anker->getName() == Name){
        node = anker;
        return true;}
    /*if ((check == NodeIDCounter) && (anker->getName() != Name))
        return false;*/
    if (anker->getLeft() != nullptr){
        node = anker->getLeft();
        if(findNode1(anker->getLeft(), Name))
            return true;
    }
    if (anker->getRight() != nullptr) {
        node = anker->getRight();
        if (findNode1(anker->getRight(), Name))
            return true;
    }
    return false;
}

bool Tree::findNode2(int PosID){
    TreeNode* tmp = anker;
    while(tmp!= nullptr){
        if(tmp->getNodePosID() == PosID)
            return true;
        if(PosID > tmp->getNodePosID())
            tmp = tmp->getRight();
        else
            tmp = tmp->getLeft();
    }
    return false;
}


void Tree::printAll(){
    preoder(this->anker);
}

//
////////////////////////////////////
//neue Funktionen:
/**
 * überprüfen, ob ein Knoten 4-Knoten ist, dann werden der Knoten in 2-Knoten umgewandelt.
 * @param Node soll ein schwarze Node sein
 * @return
 */
bool Tree:: split4Node(TreeNode*Node) {
    if (Node->getLeft() != nullptr && Node->getRight() != nullptr) {
        if (!Node->getRed() && Node->getLeft()->getRed() && Node->getRight()->getRed()) {
            if(Node == anker)
                Node->setRed(false);    //Der Wurzel is immer Schwarz
            else
                Node->setRed(true);
            Node->getLeft()->setRed(false);
            Node->getRight()->setRed(false);
            return true;
        }
    }
    return false;
}
/**
 * Rot_Schwarz Baum und 234 Baum werden in Levelorder-Reihenfolge ausgegeben.
 * queue werde benutzt.
 */
[[noreturn]] void Tree::printLevelOrder(void){
    TreeNode* parent = anker;
    queue <TreeNode> q;

    if(parent == nullptr)
        return;
    q.push(*parent);
    while(q.size()!= 0){
        *parent = q.front();
        parent->print();
        q.pop();
        if(parent->getLeft()!= nullptr) {
            q.push(*parent->getLeft());
        }
        if(parent->getRight()!= nullptr){
            q.push(*parent->getRight());
        }
    }
}
/**
 * Link-Rotation: Suchpfad mit 2 roten Verbindungen
 * a) mit der gleichen Orietierung hintereinander
 * b) mit verschiedener Orientierung (Knick)
 * @param n1 pre-Knoten des Knotens n2
 * @param n2
 * @return
 */
bool Tree::rotateTreeLeft(TreeNode* n1, TreeNode* n2) {
    TreeNode *p1 = set_parent(n1);

    /**
     * a) mit der gleichen Orietierung hintereinander
     */
    if (p1->getRight() == n1) {
        p1->setRight(n1->getLeft());
        n1->setLeft(p1);
        if (p1 != anker) {
            TreeNode *pp = set_parent(p1);
            if (pp->getLeft() == p1)
                pp->setLeft(n1);
            else
                pp->setRight(n1);
        } else
            anker = n1;
        p1->setRed(true);
        n1->setRed(false);
        return true;
    }

    /**
    * b) mit verschiedener Orientierung (Knick)
    */
    if(p1->getLeft() == n1){
        n1->setRight(n2->getLeft());
        n2->setLeft(n1);
        p1->setLeft(n2);
        rotateTreeRight(n2, n1);
        return true;
    }
    return false;
}
/**
 * Recht-Rotation: Suchpfad mit 2 roten Verbindungen
 * a) mit der gleichen Orietierung hintereinander
 * b) mit verschiedener Orientierung (Knick)
 * @param n1  n1 pre-Knoten des Knotens n2
 * @param n2
 * @return
 */
bool Tree::rotateTreeRight(TreeNode* n1, TreeNode* n2){

    TreeNode* p1 = set_parent(n1);

    /**
     * a) mit der gleichen Orietierung hintereinander
     */
    if(p1->getLeft() == n1) {
        p1->setLeft(n1->getRight());
        n1->setRight(p1);
        if (p1 != anker) {
            TreeNode *pp = set_parent(p1);
            if (pp->getLeft() == p1)
                pp->setLeft(n1);
            else
                pp->setRight(n1);
        } else
            anker = n1;
        p1->setRed(true);
        n1->setRed(false);
        return true;
    }
    /**
    * b) mit verschiedener Orientierung (Knick)
    */
    if(p1->getRight() == n1){
        n1->setLeft(n2->getRight());
        n2->setRight(n1);
        p1->setRight(n2);
        rotateTreeLeft(n2, n1);
        return true;
    }
    return false;
}


TreeNode* Tree::set_parent(TreeNode*node){
    TreeNode* tmp = anker;
    TreeNode* parent = anker;
    while(true){
        if(tmp->getNodePosID() == node->getNodePosID()) {
            break;
        }
        if(tmp->getLeft() == nullptr && tmp->getRight() == nullptr){
            break;
        }
        else if((tmp->getRight() != nullptr) && (node->getNodePosID() > tmp->getNodePosID())){
            parent = tmp;
            tmp = tmp->getRight();
        }
        else if ((tmp->getLeft() != nullptr) && (node->getNodePosID() < tmp->getNodePosID())) {
            parent = tmp;
            tmp = tmp->getLeft();
        }
    }
    return parent;
}
