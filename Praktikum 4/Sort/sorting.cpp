//
// Created by Tran Anh Minh on 05/04/2021.
//

#include "sorting.h"


namespace sorting {

    /** Quicksort
     *
     * @param arr
     * @param left
     * @param right
     */
    void QuickSort(vector<int> &arr, int left, int right) {
        if(left < right){
            int q = Partition(arr, left, right);
            QuickSort(arr, left, q-1);
            QuickSort(arr, q+1, right);
        }
    }
    int Partition(vector<int> &arr, int left, int right) {
        int centre = left + (right-left)/2;
        int tmp = 0;

        //Pivotelement mit medien method finden.
        //if left > center ->swap(left, centre)
        if(arr[left] > arr[centre]) {
            tmp = arr[left];
            arr[left] = arr[centre];
            arr[centre] = tmp;
        }
        //if right < left ->swap(right, left)
        if(arr[right] < arr[left]){
            tmp = arr[right];
            arr[right] = arr[left];
            arr[left] = tmp;
        }
        //if right < center ->swap(right, centre)
        if(arr[right] < arr[centre]){
            tmp = arr[centre];
            arr[centre] = arr[right];
            arr[right] = tmp;
        }
        tmp = arr[centre];
        arr[centre] = arr[right];
        arr[right] = tmp;

        //Partition ausführen
        int i = left - 1;
        int pivot = arr[right];

        for (int j = left; j < right; j++) {
            if (arr[j] <= pivot) {
                i = i + 1;
                //swap(A[i], A[j])
                tmp = arr[i];
                arr[i] = arr[j];
                arr[j] = tmp;
            }
        }
        //swap (A[i+1], A[right])
        tmp = arr[i + 1];
        arr[i + 1] = arr[right];
        arr[right] = tmp;
        return i + 1;
    }
    /** Merge
     *
     * @param a
     * @param b
     * @param low
     * @param pivot
     * @param high
     */
    void Merge(vector<int> &a, vector<int> &b, int low, int pivot, int high) {
        //array a in zweit gleichlängigen Arrays: L und R teilen.
        vector<int> L;
        vector<int> R;
        int i,j,k;
        int n1 = pivot-low+1;
        int n2 = high - pivot;
        for(i=0;i < n1; i++)    // Wertzuweisung für array L
            L.push_back(a[low+i]);
        for(j=0;j < n2;j++)  // Wertzuweisung für array R
            R.push_back(a[pivot+j+1]);
        //array L und R mischen -> zugeordnete Array erhälten
        i = 0;
        j = 0;
        for(k = low; k<=high; k++){
            //wenn elemen
            if(i == n1)
            {
                for(j; j<R.size();j++)
                    a[low+n1+j] = R[j];
            }
            else if(j == n2){
                for(i; i<L.size();i++)
                    a[low+n2+i] = L[i];
            }
            else if(L.at(i) <= R.at(j)){
                a.at(k) = L.at(i);
                i++;
            }else{
                a.at(k) = R.at(j);
                j++;
            }
        }
    }
    /** MergeSort
     *
     * @param a
     * @param b
     * @param low
     * @param high
     */
    void MergeSort(vector<int> &a, vector<int> &b, int low, int high) {
        if(low<high) {
            int pivot = (low + high) / 2;
            MergeSort(a, b, low, pivot);
            MergeSort(a, b, pivot + 1, high);
            Merge(a, b, low, pivot, high);
        }
    }

    /**
     * HeapSort
     * @param a
     * @param n
     */
    void HeapSort(vector<int> &a, int n) {
        int i = (n/2)-1;
        while(i >= 0){
            percDown(a,i,n);
            i--;
        }
        int j = n-1;
        while(j>0){
            //swap a[0], a[j]
            int tmp = a[0];
            a[0] = a[j];
            a[j] = tmp;
            i = j/2-1;
            for(i=j/2-1;i>=0;i--)
                percDown(a, i, j);
            j--;
        }
    }
    /**
     * percDown: MaxHeap
     * @param a
     * @param p
     * @param n
     */
    void percDown(vector<int> &a, int p, int n){
        int child = p;
        int j = p;
        int tmp = 0;
        //while lefChild(j) < n: nicht Ende des Arrays
        while(2*j+1 < n){
            child = 2*j+1;
            if(child != n-1 && a.at(child)<a.at(child+1))
                child = child+1;
            if(a.at(p)<a.at(child)) {
                tmp = a.at(child);
                a.at(child) = a.at(j);
                a.at(j) = tmp;
            }
            j = child;
        }
    }


    /**
     * ShellSort
     * @param a
     * @param n
     */
    void ShellSort(vector<int> &a, int n)
    {   int j,tmp;
        for(int gap = (n-1)/2; gap>0;gap/=2){
            for(int i = gap; i<n;i++){
                tmp = a[i];
                for(j = i; j>=gap && tmp<a[j-gap];j-=gap)
                    a[j] = a[j-gap];
                a[j] = tmp;
            }
        }
    }


    void randomizeVector(vector<int> &array, int n) {
        array.resize(n);

        for(unsigned int i=0;i<array.size();i++)
            array[i]=rand() % 1000000;
    }


}






