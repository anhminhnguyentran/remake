//
// Created by Tran Anh Minh on 24/03/2021.
//

#include "TreeNode.h"
#include <iostream>
#include <string>
#include <iomanip>


////////////////////////////////////
// Ihr Code hier:
TreeNode::TreeNode(){
    NodePosID = 0;
    NodeID = 0;
    Alter = 0;
    Einkommen = 0;
    PLZ = 0;
};
TreeNode::TreeNode(int NodePosID, int NodeID, string Name, int Alter, double Einkommen, int PLZ):
        NodePosID{NodePosID}, NodeID{NodeID}, Name{Name}, Alter{Alter}, Einkommen{Einkommen}, PLZ{PLZ}, left{nullptr},right{nullptr} {}
int TreeNode:: getNodePosID(void) {
    if(this != nullptr)
        return NodePosID;
    else
        return 0;
}
int TreeNode::getNodeID(void) {return NodeID;}
void TreeNode::setNodeID(int value) {NodeID = value;}
bool TreeNode:: getRed(){return this->red;}
void TreeNode::setRed(bool Red){this->red = Red;}
string TreeNode::getName(void) {return Name;}
void TreeNode::setName(string Name) {this->Name = Name;}
int TreeNode::getAlter(void) {return Alter;}
void TreeNode::setAlter(int Alter) {this->Alter = Alter;}
double TreeNode::getEinkommen(void) {return Einkommen;}
void TreeNode::setEinkommen(double Einkommen) {this->Einkommen = Einkommen;}
int TreeNode::getPLZ(void) {return PLZ;}
void TreeNode::setPLZ(int PLZ) {this->PLZ = PLZ;}
TreeNode* TreeNode::getLeft(void) {return left;}
TreeNode* TreeNode::getRight(void) {return right;}
void TreeNode::setLeft(TreeNode*Node) {this->left = Node;}
void TreeNode::setRight(TreeNode*Node) {this->right = Node;}
void TreeNode::print(){
    cout<<setw(3)<<this->NodeID
        <<"|"<<setw(11)<<this->Name
        <<"|"<<setw(7)<<this->Alter
        <<"|"<<setw(11)<<this->Einkommen
        <<"|"<<setw(7)<<this->getPLZ()
        <<"|"<<setw(7)<<this->getNodePosID()
        <<"| "<<this->getRed()
        <<endl;
}

//
////////////////////////////////////
