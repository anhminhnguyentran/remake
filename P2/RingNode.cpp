/*************************************************
* ADS Praktikum 2.1
* RingNode.cpp
* Erweiterung um Hilfsfunktionen gestattet.
*************************************************/
#include "RingNode.h"


// Ihr Code hier:
RingNode::RingNode(){};
RingNode::RingNode(int old, std::string desc, std::string symb):OldAge{old}, Description{desc},SymbolicData{symb} {}
int RingNode:: getAge(){return this->OldAge;}
void RingNode::setAge(int age){this->OldAge = age;}
std::string RingNode::getDescription(){return this->Description;}
void RingNode::setDescription(std::string description){this->Description = description;}
std::string RingNode::getData(){return this->SymbolicData;}
void RingNode::setData(std::string data){this->SymbolicData = data;}
RingNode* RingNode::getNext(){return this->next;}
void RingNode::setNext(RingNode*node){this->next = node;}

// 
////////////////////////////////////