//
// Created by Tran Anh Minh on 20/03/2021.
//

#ifndef BINAERBAUM_TREE_H
#define BINAERBAUM_TREE_H
/*************************************************
* ADS Praktikum 2.2
* Tree.h
* Erweiterung um Hilfsattribute und -funktionen gestattet, wenn erforderlich.
*************************************************/
#pragma once
#include <string>
#include "TreeNode.h"
#include "catch.hpp"

using namespace std;

class Tree{

private:
    ///////////////////////////////////////
    // Ihr Code hier:
    TreeNode* anker;
    TreeNode* node;
    int NodeIDCounter;

    //
    ////////////////////////////////////

public:
    ///////////////////////////////////////
    // Ihr Code hier:
    Tree();
    void addNode(string Name, int Alter, double Einkommen, int PLZ);
    void deleteNode(int NodePosID);
    bool searchNode(string Name);
    void printAll();

    // zugefuegene Funktionen:
    TreeNode* getNode(){return this->node;}
    void preoder(TreeNode*anker);
    bool findNode1(TreeNode*anker, string Name);
    bool findNode2(int PosID);
    TreeNode*findNode3(TreeNode*anker, string Name);
    //
    ////////////////////////////////////
    // friend-Funktionen sind für die Tests erforderlich und müssen unangetastet bleiben!
    friend TreeNode * get_anker(Tree& TN);
};

#endif //BINAERBAUM_TREE_H
