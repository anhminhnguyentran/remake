//
// Created by Tran Anh Minh on 24/03/2021.
//

#ifndef RSBAUM_TREE_H
#define RSBAUM_TREE_H

#pragma once
#include <string>
#include "TreeNode.h"
#include "catch.h"

using namespace std;

class Tree{

private:
    ///////////////////////////////////////
    // Ihr Code hier:
    TreeNode* anker;
    TreeNode* node;

    int NodeIDCounter;

    //
    ////////////////////////////////////

public:
    ///////////////////////////////////////
    // Ihr Code hier:
    Tree();
    bool addNode(string Name, int Alter, double Einkommen, int PLZ);
    //void deleteNode(int NodePosID);
    bool searchNode(string Name);
    void printAll();

    // zugefuegene Funktionen:
    TreeNode* getNode(){return this->node;}
    void preoder(TreeNode*anker);
    bool findNode1(TreeNode*anker, string Name);
    bool findNode2(int PosID);

    ////////////////////////////////////
    //neue Methode:

    bool split4Node(TreeNode*Node);

    [[noreturn]] void printLevelOrder(void);
    bool rotateTreeRight(TreeNode*n1, TreeNode*n2);
    bool rotateTreeLeft(TreeNode*n1, TreeNode*n2);
    TreeNode* set_parent(TreeNode*node);
    ///////////////////////////////////

    // friend-Funktionen sind für die Tests erforderlich und müssen unangetastet bleiben!
    friend TreeNode * get_anker(Tree& TN);
};


#endif //RSBAUM_TREE_H

