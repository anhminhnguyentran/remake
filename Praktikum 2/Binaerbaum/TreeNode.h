//
// Created by Tran Anh Minh on 20/03/2021.
//

#ifndef BINAERBAUM_TREENODE_H
#define BINAERBAUM_TREENODE_H
/*************************************************
* ADS Praktikum 2.2
* TreeNode.h
* Erweiterung um Hilfsattribute und -funktionen gestattet, wenn erforderlich.
*************************************************/
#pragma once
#include <string>

using namespace std;

class TreeNode{

private:
    ///////////////////////////////////////
    // Ihr Code hier:
    int NodePosID;
    int NodeID;
    string Name;
    int Alter;
    double Einkommen;
    int PLZ;
    TreeNode* left;
    TreeNode* right;
    //
    ////////////////////////////////////

public:
    ///////////////////////////////////////
    // Ihr Code hier:
    TreeNode();
    TreeNode(int NodePosID, int NodeID, string Name, int Alter, double Einkommen, int PLZ);
    int getNodePosID(void);
    void setNodePosID(int value);
    int getNodeID(void);
    void setNodeID(int value);
    string getName(void);
    void setName(string Name);
    int getAlter(void);
    void setAlter(int Alter);
    double getEinkommen(void);
    void setEinkommen(double Einkommen);
    int getPLZ(void);
    void setPLZ(int PLZ);

    TreeNode*getLeft(void);
    TreeNode*getRight(void);
    void setLeft(TreeNode*Node);
    void setRight(TreeNode*Node);
    void print();

    //
    ////////////////////////////////////
};

#endif //BINAERBAUM_TREENODE_H
