#define CATCH_CONFIG_RUNNER
#include "catch.h"
#include <iostream>
#include "Tree.h"


int main() {
    int result = Catch::Session().run();
    /*
    cout<<"Das ist mein drittes Praktikum!!!"<<endl;
    Tree test;
    test.addNode("Mayer", 20, 0, 0);
    test.addNode("Mayer2", 10, 0, 0);
    test.addNode("Mayer3", 35, 0, 0);
    test.addNode("Mayer4", 26, 0, 0);
    test.addNode("Mayer5", 40, 0, 0);
    test.addNode("Mayer6", 25, 0, 0);
    test.addNode("Mayer7", 30, 0, 0);
     test.printAll();
     */
    cout<<"================================= \n"
        <<"Person Analyzer v19.84, by George Orwell\n"
        <<"1) Datensatz einfuegen, manuell\n"
        <<"2) Datensatz einfuegen, CSV Datei \n"
        <<"3) Datensatz loeschen\n"
        <<"4) Suchen \n"
        <<"5) Ausgabe in Preorder \n"
        <<"6) Ausgabe in Levelorder \n"
        <<"?>"
        <<endl;

    Tree test;
    string name;
    int choice, age, plz, PosID;
    double salary;

    while(true){
        cin >> choice;
        if(choice == 1) {
            cout<< "?> 1 \n"
                << " + Bitte geben Sie die den Datensatz ein"<<endl;
            cout<< "Name ?> ";
            cin >> name;
            cout<< "Alter ?> ";
            cin >> age;
            cout<<"Einkommen ?>";
            cin >> salary;
            cout<<"PLZ ?>";
            cin >> plz;

            test.addNode(name, age, salary, plz);
            cout <<"+ Ihr Datensatz wurde eingefuegt"<<endl;
        }
        if(choice == 2){
            string Wahl, Age, Salary, Plz;

            cout<< "?> 2 \n"
                <<"+ Moechten Sie die Daten aus der Datei ExportZielanalyse.csv importieren (j/n) ?>";
            cin>>Wahl;
            if(Wahl == "j") {
                ifstream csv_file("C:/Users/Tran Anh Minh/Documents/FH Aachen/ADS/Praktikum 2/Binaerbaum/ExportZielanalyse.txt"); //input file stream
                if (bool(csv_file) == false)
                    cout << "Fehler beim Oeffnung des Files!!!" << endl;
                else {
                    while (true) {

                        getline(csv_file, name, ';');
                        getline(csv_file, Age, ';');
                        getline(csv_file, Salary, ';');
                        getline(csv_file, Plz, '\n');
                        if (csv_file.eof()) break;

                        test.addNode(name, stoi(Age), stoi(Salary), stoi(Plz));
                    }
                    csv_file.close(); //hier ist Methode close() nicht wirklick notwendig, weil csv_file eine lokale Variable ist.
                    cout << "+ Daten wurden dem Baum hinzugefuegt." << endl;
                }
            }
            else if (Wahl == "n")
                cout<<"Keine Datei werde automatisch eingefuegt!!!"<<endl;
        }

        if(choice == 3){
            cout<<"?> 3 \n"
                <<"+ Bitte geben Sie zuloeschenden Datensatz an.\n"
                <<"PosID ?>";

            //cin >> PosID;
            if(!(cin>>PosID)) {
                cin.clear();
                cin.ignore(numeric_limits<streamsize>::max(), '\n');
            }
            if(test.findNode2(PosID)) {
                //test.deleteNode(PosID); hier wird keine Löschung von Konten ausgefürht.
                cout << "Datensatz wurde geloescht." << endl;
            }
            else
                cout<<"es besteht keine Datensatz mit ensprechendem PosID "<<endl;
        }
        if(choice == 4){
            cout<<"+ Bitte geben Sie den zu suchenden Datensatz an\n";
            cout<<"Name ?>";

            //cin >> name;
            if(!(cin>>name)) {
                cin.clear();
                cin.ignore(numeric_limits<streamsize>::max(), '\n');
            }
            if(test.searchNode(name)) {
                TreeNode* node = test.getNode();
                cout << "Fundstellen:\n"
                     <<"NodeID: "<<node->getNodeID()
                     <<", Name: "<<node->getName()
                     <<", Alter: "<<node->getAlter()
                     <<", Einkommen: "<<node->getEinkommen()
                     <<", PLZ: "<<node->getPLZ()
                     <<", PosID: "<<node->getNodePosID()
                     <<endl;
            }
            else
                cout<<"Datei wurde nicht gefunden haben können."<<endl;
        }
        if(choice == 5){
            cout<<"Ausgabe in Preorder als binaerer Suchbaum:\n"
                <<"ID | Name      | Alter | Einkommen |  PLZ  | Pos\n"
                <<"---+-----------+-------+-----------+-------+--------\n";
            test.printAll();
        }
        if(choice == 6) {
            cout <<"Ausgabe in Levelorder als binaerer Suchbaum:\n"
                 <<"ID | Name      | Alter | Einkommen |  PLZ  | Pos   | Red\n"
                 <<"---+-----------+-------+-----------+-------+-------+-------\n\n";
            test.printLevelOrder();

            cout<<"Ausgabe in Levelorer als 234-Baum:\n";
        }
        if(choice == 0)
            break;
    }
    system("PAUSE");
    return 0;
}


