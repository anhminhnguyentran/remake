//
// Created by Tran Anh Minh on 24/03/2021.
//

#ifndef RSBAUM_TREENODE_H
#define RSBAUM_TREENODE_H
#include <string>
using namespace std;

//Class TreeNode
class TreeNode{

private:
    ///////////////////////////////////////
    // Ihr Code hier:
    int NodePosID;
    int NodeID;
    bool red;
    string Name;
    int Alter;
    double Einkommen;
    int PLZ;
    TreeNode* left;
    TreeNode* right;
    //
    ////////////////////////////////////

public:
    ///////////////////////////////////////
    // Ihr Code hier:
    TreeNode();
    TreeNode(int NodePosID, int NodeID, string Name, int Alter, double Einkommen, int PLZ);
    int getNodePosID(void);
    int getNodeID(void);
    void setNodeID(int value);
    bool getRed();
    void setRed(bool Red);
    string getName(void);
    void setName(string Name);
    int getAlter(void);
    void setAlter(int Alter);
    double getEinkommen(void);
    void setEinkommen(double Einkommen);
    int getPLZ(void);
    void setPLZ(int PLZ);

    TreeNode*getLeft(void);
    TreeNode*getRight(void);
    void setLeft(TreeNode*Node);
    void setRight(TreeNode*Node);
    void print();

    //
    ////////////////////////////////////
};

#endif //RSBAUM_TREENODE_H
