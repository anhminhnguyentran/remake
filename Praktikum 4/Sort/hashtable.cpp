//
// Created by Tran Anh Minh on 05/04/2021.
//

#include "hashtable.h"
#include <iostream>

using namespace std;

HashTable::HashTable(int size) {
    this->size = size;
    this->elements = 0;
    this->collisionCount = 0;
    this->hashTable = new vector<int>;
    this->hashTable->resize(size);
    this->hashTable->assign(size,-1);
}

HashTable::~HashTable()
{
    this->hashTable->clear();
    elements = 0;
}

int HashTable::hashValue(int item) {
    int index;
    if(item!=-1)
        index = item % this->size;
    return index;
}

int HashTable::insert(int item) {

    //******************************************
    // implement insertion of new element here *
    //******************************************
    int index = hashValue(item);
    // Falls Collisions beitretten->neue Index berechnen
    int i=1;
    while(hashTable->at(index)!=-1) {
        index = (item + i) % size;
        collisionCount++;
        i++;
    }
    hashTable->at(index) = item;
    elements++;
    return 0; //dummy return
}

int HashTable::at(int i) {
    return hashTable->at(i);
}

int HashTable::getCollisionCount() {
    return this->collisionCount;
}

int HashTable::getSize() {
    return this->size;
}

int HashTable::getElements() {
    return this->elements;
}

