cmake_minimum_required(VERSION 3.17)
project(RSbaum)

set(CMAKE_CXX_STANDARD 14)

add_executable(RSbaum main.cpp TreeNode.cpp TreeNode.h Tree.cpp Tree.h TreeTest.cpp catch.h)